import React from "react";
import ReactDOM from "react-dom/client";
import './index.css';

function App() {
    const initialValues = {
        firstname: '',
        lastname: '',
        age: '',
        employee: false,
        color: '',
        souces: [],
        stooge: 'Larry',
        note: '',
        disabled: true
    }
    const [state, setState] = React.useState(initialValues);

    const [error, setError] = React.useState(Object.keys(initialValues).map((key) => {
        return { key: false }
    }));

    function handleValidation() {
        let formIsValid = true;

        // Name
        if (typeof state.firstname !== 'undefined' && !state.lastname !== 'undefined') {
            if (!state.firstname.match(/^[a-zA-Z]+$/) && !state.lastname.match(/^[a-zA-Z]+$/)) {
                formIsValid = false;
                setError((error) => ({
                    ...error,
                    firstname: true,
                    lastname: true
                }))
            }
        }

        // Age
        if (typeof state.age !== 'undefined') {
            if (!state.age.match(/^[0-9]/)) {
                formIsValid = false;
                setError((error) => ({
                    ...error,
                    age: true,
                }))
            }
        }

        // Notes
        if (typeof state.note !== 'undefined') {
            if (state.note.length > 100) {
                formIsValid = false;
                setError((error) => ({
                    ...error,
                    note: true,
                }))
            }
        }

        return formIsValid;
    }

    function handleSubmit(e) {
        e.preventDefault();

        if (handleValidation()) {
            alert(JSON.stringify(state))

        }
    }

    function reset() {
        setState(initialValues)
        window.location.reload(false)
    }

    function handleChange(event) {
        const target = event.target;
        const name = target.name;
        const value = target.type === "checkbox" && name !== 'souces' ? target.checked : target.value;

        setState((state) => ({
            ...state,
            disabled: false,
            [name]: value
        }));
    }

    function handleMultipleCheckboxChange(event) {
        let name = event.target.name
        let value = event.target.value

        let sel = state[name]

        if (sel.includes(value)) {
            let find = sel.indexOf(value)
            if (find > -1) {
                sel.splice(find, 1)
            }
        } else {
            sel.push(value)
        }

        setState((state) => ({
            ...state,
            [name]: sel,
        }))
    }

    return (
        <div className='container'>
            <form onSubmit={handleSubmit}>
                <div className='field'>
                    <label className='label' htmlFor='First Name'>First Name</label>
                    <div className='control'>
                        <input className={error.firstname ? 'error' : null} type='text' name='firstname' onChange={handleChange} value={state.firstname} placeholder='First Name' />
                    </div>
                </div>

                <div className='field'>
                    <label className='label' htmlFor='Last Name'>Last Name</label>
                    <div className='control'>
                        <input className={error.lastname ? 'error' : null} type='text' name='lastname' onChange={handleChange} value={state.lastname} placeholder='Last Name' />
                    </div>
                </div>

                <div className='field'>
                    <label className='label' htmlFor='Age'>Age</label>
                    <div className='control'>
                        <input className={error.age ? 'error' : null} type='text' name='age' onChange={handleChange} value={state.age} placeholder='Age' />
                    </div>
                </div>

                <div className='field'>
                    <label className='checkbox' htmlFor='Employee'>Employee</label>
                    <div className='control'>
                        <input type='checkbox' name='employee' onChange={handleChange} value={state.employee} placeholder='Employee' checked={state.employee === true} />
                    </div>
                </div>

                <div className='field'>
                    <label className='label' htmlFor='Color'>Color</label>
                    <div className='control'>
                        <select name='color' onChange={handleChange} value={state.color}>
                            <option value=''></option>
                            <option value='orange'>Orange</option>
                            <option value='yellow'>Yellow</option>
                            <option value='green'>Green</option>
                            <option value='purple'>Purple</option>
                        </select>
                    </div>
                </div>

                <div className='field'>
                    <label className='label' htmlFor='Souce'>Souce</label>
                    <div className='control'>
                        <label className='checkbox'>
                            <input type='checkbox' name='souces' value='ketchup' onChange={handleMultipleCheckboxChange} />
                            Ketchup
                        </label>
                        <label className='checkbox'>
                            <input type='checkbox' name='souces' value='mustard' onChange={handleMultipleCheckboxChange} />
                            Mustard
                        </label>
                        <label className='checkbox'>
                            <input type='checkbox' name='souces' value='mayonnaise' onChange={handleMultipleCheckboxChange} />
                            Mayonnaise
                        </label>
                        <label className='checkbox'>
                            <input type='checkbox' name='souces' value='guacamole' onChange={handleMultipleCheckboxChange} />
                            Guacamole
                        </label>
                    </div>
                </div>

                <div className='field'>
                    <label className='label' htmlFor='Best Stooge'>Best Stooge</label>
                    <div className='control'>
                        <label className='radio'>
                            <input type='radio' name='stooge' value='Larry' className="radio" onChange={handleChange} checked={state.stooge === 'Larry'} />
                            Larry
                        </label>
                        <label className='radio'>
                            <input type='radio' name='stooge' value='Moe' className="radio" onChange={handleChange} checked={state.stooge === 'Moe'} />
                            Moe
                        </label>
                        <label className='radio'>
                            <input type='radio' name='stooge' value='Curly' className="radio" onChange={handleChange} checked={state.stooge === 'Curly'} />
                            Curly
                        </label>
                    </div>
                </div>

                <div className='field'>
                    <label className='label' htmlFor='Note'>Notes</label>
                    <div className='control'>
                        <textarea className={error.note ? 'error' : null} type='textarea' name='note' placeholder='Note' value={state.note} onChange={handleChange}>
                        </textarea>
                    </div>
                </div>

                <div className='field'>
                    <div className='control'>
                        <button disabled={state.disabled}>Submit</button>
                        <button onClick={reset} disabled={state.disabled}>Reset</button>
                    </div>
                </div>

                <div className="output">
                    &#123;
                    {Object.entries(state).map(([key, value]) => {
                        if (value !== '' && key !== 'disabled') {
                            return <p key={key}>{JSON.stringify(key)}: {JSON.stringify(value)}</p>
                        }
                    })}
                    &#125;
                </div>
            </form>
        </div>
    )
}

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <App />


);
