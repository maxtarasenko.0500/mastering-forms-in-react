import React from 'react';
import ReactDOM from 'react-dom/client';
import { Field, Form, withFormik } from 'formik';
import * as yup from 'yup'
import './index.css';

const formAlert = () => {
  const data = document.querySelector('.output').innerHTML
  console.log(data);
}

const App = (props) => {
  const {
    touched,
    handleChange,
    handleReset,
    isSubmitting,
  } = props;

  return (
    <div className='container'>
      <Form>
        <div className='field'>
          <label className='label' htmlFor='First Name'>First Name</label>
          <div className='control'>
            {touched.firstname}
            <Field className='input' type='text' name='firstname' placeholder='First Name' />
          </div>
        </div>

        <div className='field'>
          <label className='label' htmlFor='Last Name'>Last Name</label>
          <div className='control'>
            {touched.lastname}
            <Field className='input' type='text' name='lastname' placeholder='Last Name' />
          </div>
        </div>

        <div className='field'>
          <label className='label' htmlFor='Age'>Age</label>
          <div className='control'>
            {touched.age}
            <Field className='input' type='text' name='age' placeholder='Age' />
          </div>
        </div>

        <div className='field'>
          <label className='checkbox' htmlFor='Employee'>Employee</label>
          <div className='control'>
            {touched.employee}
            <Field type='checkbox' name='employee' placeholder='Employee' />
          </div>
        </div>

        <div className='field'>
          <label className='label' htmlFor='Color'>Color</label>
          {touched.color}
          <div className='control'>
            <Field component='select' name='color'>
              <option value=''></option>
              <option value='orange'>Orange</option>
              <option value='yellow'>Yellow</option>
              <option value='yellow'>Green</option>
              <option value='yellow'>Yellow</option>
              <option value='yellow'>Purple</option>
            </Field>
          </div>
        </div>

        <div className='field'>
          <label className='label' htmlFor='Souce'>Souce</label>
          <div className='control'>
            <label className='checkbox'>
              <Field type='checkbox' name='souces' value='ketchup' />
              Ketchup
            </label>
            <label className='checkbox'>
              <Field type='checkbox' name='souces' value='mustard' />
              Mustard
            </label>
            <label className='checkbox'>
              <Field type='checkbox' name='souces' value='mayonnaise' />
              Mayonnaise
            </label>
            <label className='checkbox'>
              <Field type='checkbox' name='souces' value='guacamole' />
              Guacamole
            </label>
          </div>
        </div>

        <div className='field'>
          <label className='label' htmlFor='Best Stooge'>Best Stooge</label>
          <div className='control'>
            <label className='radio'>
              <input type='radio' name='stooge' value='Larry' className="radio" onChange={handleChange} checked />
              Larry
            </label>
            <label className='radio'>
              <input type='radio' name='stooge' value='Moe' className="radio" onChange={handleChange} />
              Moe
            </label>
            <label className='radio'>
              <input type='radio' name='stooge' value='Curly' className="radio" onChange={handleChange} />
              Curly
            </label>
          </div>
        </div>

        <div className='field'>
          <label className='label' htmlFor='Note'>Notes</label>
          <div className='control'>
            <input type='textarea' className='note' name='note' placeholder='Notes' onChange={handleChange}>
            </input>
          </div>
        </div>

        <div className='field'>
          <div className='control'>
            <button type='submit' disabled={isSubmitting} onClick={formAlert}>Submit</button>
            <button onClick={handleReset}>Reset</button>
          </div>
        </div>

        <FormikOutput {...props} />
      </Form>
    </div>
  )
}

const FormikOutput = (props) => {
  return (<div className='output'>
    {
      Object.keys(props.values).map((key) => {
        if (props.values[key] !== '' && props.values[key] !== null && props.values[key] !== undefined) {
          // return JSON.stringify(props[key])
          return <div key={key}>{`${key}: ${props.values[key]}`}</div>
        }
      })
    }
  </div>)
}

const FormikApp = withFormik({
  mapPropsToValues({
    firstname,
    secondname,
    age,
    employee,
    color,
    souces,
    stooge,
    note,
  }) {
    return {
      firstname: firstname || '',
      lastname: secondname || '',
      age: age || '',
      employee: employee || false,
      color: color || '',
      souces: souces || '',
      stooge: stooge || 'Larry',
      note: note || '',
    }
  },

  validationSchema: yup.object().shape({
    firstname: yup.string().matches(/^[a-zA-Z ]*$/).required('1'),
    lastname: yup.string().matches(/^[a-zA-Z ]*$/).required('1'),
    note: yup.string().max(100, 'Text should not exceed more than 100 characters').required()
  }),

  handleSubmit(setSubmitting) {
    console.log('e')
    setSubmitting(false)
  },

  submitForm()

})(App)

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <FormikApp />
);
